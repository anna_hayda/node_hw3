const mongoose = require('mongoose');

const truckSchema = new mongoose.Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    default: null,

  },
  type: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    default: 'IS',
    required: true,
  },
  dimensions: {
    type: Object,
    width: {
      type: Number,
    },
    length: {
      type: Number,
    },
    hight: {
      type: Number,
    },
  },
  payload: {
    type: Number,
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },

});


module.exports.Truck = mongoose.model('Truck', truckSchema);
